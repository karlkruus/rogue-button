// Get the button element from id
const button = document.getElementById('button');

// The width and height for the button
const bWidth = 100;
const bHeight = 100;
// This determines how fast the button is going to move away from the mouse
const moveSpeed = 50;

// These are going to be used to control the margins of the button
let marginLeft = 0;
let marginTop = 0;

// Set the width and height of the button
button.style.width = bWidth + 'px';
button.style.height = bHeight + 'px';
// Set the button position to absolute, allowing control on the top and left css properties
button.style.position = 'absolute';

// Center the button on page load
centerButton();
// When the window is resized, call the centerButton function
window.onresize = centerButton;

// This function is used to center the button
function centerButton() {
    // Get left and top by taking the total size of the window, subtracting the button size and dividing that in half.
    const left = (window.innerWidth - bWidth) / 2;
    const top = (window.innerHeight - bHeight) / 2;

    // Set the left and top properties
    button.style.left = left + 'px';
    button.style.top = top + 'px';
}

// This function is called when the user moves the cursor over the button element
button.onmouseover = function(event) {
    // Get the center coordinates of the button.
    buttonCenterX = button.offsetLeft + bWidth / 2,
    buttonCenterY = button.offsetTop +  bHeight / 2

    // Get the coordinates of the mouse
    const mousePosX = event.clientX; 
    const mousePosY = event.clientY;

    // When the mouse is to the left of the button center, move the button right
    if (buttonCenterX > mousePosX) marginLeft += moveSpeed;
    // Otherwise move it left
    else marginLeft -= moveSpeed;

    // When the mouse is higher than the button center, move the button down
    if (buttonCenterY > mousePosY) marginTop += moveSpeed;
    // Otherwise move it up
    else marginTop -= moveSpeed;

    // Set the margins
    button.style.marginLeft = marginLeft + 'px';
    button.style.marginTop = marginTop + 'px';
}
